<?php

function node_agreement_views_data() {
  $data['node_agreement']['table']['group']  = t('Node agreement');

  $data['node_agreement']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['node_agreement']['accessible'] = array(
    'title' => t('Accessible'),
    'help' => t('Whether the node is accessible (either because its agreement has been signed by the current user or because it has no agreement to start with).'),
    'filter' => array(
      'handler' => 'views_handler_filter_node_agreement_accessible',
    ),
  );

  return $data;
}

function node_agreement_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'node_agreement'),
    ),
    'handlers' => array(
      'views_handler_filter_node_agreement_accessible' => array(
        'parent' => 'views_handler_filter',
      ),
    ),
  );
}

