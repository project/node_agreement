<?php

class views_handler_filter_node_agreement_accessible extends views_handler_filter {
  function admin_summary() { }
  function operator_form() { }

  function query() {
    $table = $this->ensure_my_table();
    $this->query->add_where($this->options['group'], "$table.aid IS NULL OR EXISTS (SELECT * FROM {node_agreement_signature} nas where $table.nid = nas.nid and nas.uid = ***CURRENT_USER***)");
  }
}

